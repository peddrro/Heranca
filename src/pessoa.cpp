#include "pessoa.hpp"
#include "aluno.hpp"
#include <iostream>
#include <string>

using namespace std;

Pessoa::Pessoa(){
        nome = "";
        idade = "";
        telefone = "";
}

Pessoa::Pessoa(string nome, string idade, string telefone){
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
}

string  Pessoa::getNome() {
        return nome;
}

void Pessoa::setNome(string nome) {
        this->nome = nome;
}

string  Pessoa::getIdade() {
        return idade;
}

void Pessoa::setIdade(string idade) {
        this->idade = idade;
}

string  Pessoa::getTelefone() {
        return telefone;
}

void Pessoa::setTelefone(string telefone) {
        this->telefone = telefone;
}

Aluno::Aluno(){

}
Aluno::Aluno(string nome, string idade, string telefone, int matricula, int quantidadeCred, int semestre, float ira)
        : Pessoa(nome,idade,telefone){
        setMatricula(matricula);
        setQuantidadeCreditos(quantidadeCred);
        setSemestre(semestre);
        setIra(ira);
}

int Aluno::getMatricula(){
        return matricula;
}

void Aluno::setMatricula(int matricula){
        this->matricula = matricula;
}

int Aluno::getQuantidadeCreditos(){
        return quantidadeCred;
}

void Aluno::setQuantidadeCreditos(int creditos){
        quantidadeCred = creditos;
}

int Aluno::getSemestre(){
        return semestre;
}

void Aluno::setSemestre(int semestre){
        this->semestre = semestre;
}

float Aluno::getIra(){
        return ira;
}

void Aluno::setIra(float ira){
        this->ira = ira;
}
