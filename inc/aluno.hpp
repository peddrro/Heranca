#ifndef ALUNO_H
#define ALUNO_H

#include "pessoa.hpp"
#include <iostream>
#include <string>

using namespace std;

class Aluno: public Pessoa{

	private: 
		int matricula;
		int quantidadeCred;
		int semestre;
		float ira;
	public:
		Aluno();
		Aluno(string, string, string, int matricula, int quantidadeCred, int semestre, float ira);
		void setMatricula(int matricula);
		int getMatricula();
		void setQuantidadeCreditos(int creditos);
		int getQuantidadeCreditos();
		void setSemestre(int semestre);
		int getSemestre();
		void setIra(float ira);
		float getIra();
};

#endif